## simple start

1. make a folder in your id directory

    toggle-switch

2. open toggle-swtich project in MS VS code 1.29 version

3. prepare node project with npm

    npm init -y 

4. make a file named `toggle-switch.js`

5. open the file `toggle-switch.js`

6. update `package.json` for `npm start` script

    "start": "node toggle-switch.js"

7. let's code!

## advanced topic

1. remove global status variables

2. use closure

### sample code

```
const toggler = () => {
  let stat = 1
  return () => {
    stat = stat ? 0 : 1
    return stat
  }
}
const toggle = toggler()
setInterval(() => {
  console.log(toggle())
}, 1000)
```