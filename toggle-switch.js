const readline = require('readline')

const title = 'hello button toggler simulator'
const title_underline = '==============================\n'
// const question = 'press \'b\' key for button simulation'
// escape charactor
const question = `press 'b' key for button simulation: `

let ledStatus = {
    led1: 1,
    led2: 0
}

let buttonStatus = {
    button1: 0,
    button2: 0
}

console.log(title)
console.log(title_underline)

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

runner()

function runner() {
    rl.question(question, (answer) => {
        console.log(`a button that accepted: ${answer}`)

        if (answer == 'q') {
            quitSimulation(function () {
                rl.close()
            })
        } else if (answer == 'b') {
            toggle(function () {
                runner()
            })
        } else {
            runner()
        }
    })
}

function quitSimulation(callback) {
    console.log('end of simulation')

    callback()
}

function toggle(callback) {
    ledSimulator('read', (state) => {
        let newState = state == 1 ? 0 : 1

        ledSimulator('write', newState, (state) => {
            console.log('led state:', state)
            console.log('led is toggled')

            callback()
        })
    })
}

function ledSimulator(command, newValue, callback) {
    if (!callback && (typeof newValue == 'function' || typeof newValue == 'object')) {
        callback = newValue
    }

    const threshold = 0.5
    let state = 0

    switch (command) {
        case 'random':
            state = Math.random() > threshold ? 1 : 0

            break
        case 'read':
            state = ledStateReader('led1')
            // state = ledStatus.led1
            // state = ledStatus['led1']

            break
        case 'write':
            state = ledStateWriter('led1', newValue)

            break
        default:
            console.error('command not found!')

            break
    }

    callback && callback(state)

    function ledStateReader(ledName) {
        console.log('read led state from', ledName)

        // defense code for exception
        if (!(ledName in ledStatus)) throw new Error('no led name assigned')

        return ledStatus[ledName]
    }

    function ledStateWriter(ledName, value) {
        console.log('write led state to', ledName, 'by', value)

        if (!(ledName in ledStatus)) throw new Error('no led name assigned')

        ledStatus[ledName] = value

        return value
    }
}
